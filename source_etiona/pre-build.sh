#!/bin/sh

set -e

UTILDIR=utils/

# update keywords and popcon
for dir in ./menu-data
do
	echo "adding keywords"
	$UTILDIR/addKeywords.py -d $dir
	echo "adding popcon"
	$UTILDIR/addPopconData.py -d $dir
done
