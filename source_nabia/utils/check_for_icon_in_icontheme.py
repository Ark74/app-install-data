#!/usr/bin/python

import os
import subprocess
import sys


# FIXME: calculate this automatically from software-centers dependencies
DEPENDANT_ICON_THEMES = ["humanity-icon-theme",
                         "gnome-icon-theme",
                         "hicolor-icon-theme",
                         ]

if __name__ == "__main__":
    
    icondir = sys.argv[1]
    
    icons_in_icontheme = set()
    # gather data from the icon themes we have
    for pkgname in DEPENDANT_ICON_THEMES:
        output = subprocess.check_output(["dpkg", "-L", pkgname])
        for line in output.split("\n"):
            if not line.startswith("/usr/share/icons"):
                continue
            iconname = os.path.basename(line)
            icons_in_icontheme.add(iconname)

    # now go over icondir
    seen = set()
    for f in os.listdir(icondir):
        if f in icons_in_icontheme:
            print "in icontheme: ", f
        if os.path.splitext(f)[0] in seen:
            print "duplicated: ", f
        seen.add(os.path.splitext(f)[0])

