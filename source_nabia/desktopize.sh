#!/bin/bash
#
# written by Gabor Kelemen 
# 
# desktopize.sh: 
# - turn desktop files back to desktop.in files
# - genereate pot template
# - then turn back desktop.in files to desktop files

DIRS="\
menu-data"

for d in $DIRS; do 

	echo "rename .desktop files in $d..."
	rename s/.desktop$/\.desktop\.in/g $d/*

	echo "marking translatable values as such in $d..."
	for repl in Name Comment X-GNOME-FullName; do
		sed -i s/^$repl=/_$repl=/g $d/*desktop.in
	done
	# X-GNOME-FullName takes priority in s-c, if it's present, translating Name is unnecessary.
	# We leave Name marked for translation if there was no X-G-FN key
	for file in $d/*desktop.in ; do
		if [ `grep -c _X-GNOME-FullName= $file` -eq 1 ]; then
			sed -i s/^_Name=/Name=/g $file;
		fi
	done
	# some .desktop files have only GenericName instead of Comment (KDE4), while some have both (KDE3)
	for file in $d/*desktop.in ; do
		if [ `grep -c _Comment $file` -eq 0 ]; then
			sed -i s/^GenericName=/_GenericName=/g $file;
		fi
	done
done

echo "populating potfiles.in and creating pot file..."
cd po
echo "[encoding: UTF-8]" >> POTFILES.in
intltool-update -m 2> /dev/null
cat missing >> POTFILES.in
rm missing
# generate a pot...
intltool-update -p -g app-install-data
# update existing translations 
# - dunno if this is necessary or not, but we are already here, so why not? :)
# this can become quite slow with time, however.
cat LINGUAS | while read lang; do 
    intltool-update -g app-install-data $lang
done
cd ..

# now restore the desktop files...
for d in $DIRS; do

	echo "rename .desktop.in files in $d..."
	rename s/desktop\.in$/desktop/g $d/*

	echo "unmark translatable values in $d..."
	for repl in Name Comment GenericName X-GNOME-FullName; do
		sed -i s/^_$repl=/$repl=/g $d/*.desktop
	done
	# we also replace translation domains...
	echo "should be any gettext-domain defined in the desktop files, we dont need it"
	sed -i s/^X-Ubuntu-Gettext-Domain=.*//g $d/*.desktop

	echo "instead, add our own domain to the files, this will be interesting later"
	for i in $d/*desktop ; do
		echo "X-Ubuntu-Gettext-Domain=app-install-data" >> $i
	done

done

